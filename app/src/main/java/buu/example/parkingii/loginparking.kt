package buu.example.parkingii


import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.navigation.findNavController
import kotlinx.android.synthetic.main.fragment_loginparking.*
import buu.example.parkingii.databinding.FragmentLoginparkingBinding
import kotlinx.android.synthetic.main.fragment_loginparking.view.*
import kotlinx.android.synthetic.main.fragment_loginparking.view.user_edit_name

/**
 * A simple [Fragment] subclass.
 */
class loginparking : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val binding: FragmentLoginparkingBinding = DataBindingUtil.inflate(inflater,R.layout.fragment_loginparking,container,false)
        binding.btnLoginCat.setOnClickListener{View ->
            binding.apply{
                if(user_edit_name.text.toString() == "1234" && pass_edit_password.text.toString() == "1234") {
                    View.findNavController().navigate(R.id.action_loginparking_to_praking2)
                }
            }
        }
       return binding.root

    }


}
